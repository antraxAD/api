//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

var movimientosJSONv2 = require('.//movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);


app.get('/',function(req, res) {
  //res.send("Hola Mundo, desde Node.js");
  res.sendFile(path.join(__dirname,'index.html'));
} )

app.post('/', function(req, res) {
  res.send("Hemos recibido su petición Post");
})

//se agregan peticiones PUT y DELETE
app.put('/', function(req, res) {
  res.send("Hemos recibido su petición Put");
})

app.delete ('/', function(req, res) {
  res.send("Hemos recibido su petición DELETE");
})

app.get('/Clientes',function(req,res) {
  res.send("Aqui tiene a los clientes");
})

app.get('/Clientes/:idcliente',function(req,res) {
  res.send("Aqui tiene al cliente:" + req.params.idcliente);
})

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosJSONv2);
})

//para regresar un solo registro
app.get('/v2/Movimientos/:id/:nombre', function(req, res) {
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send(movimientosJSONv2[req.params.id-1]);
  //res.send(movimientosJSONv2[req.params.id-1].ciudad);
})

//nueva peticion get
app.get('/v2/Movimientosq', function(req, res) {
  console.log(req.query);
  res.send("Petición con query recibida desde Browser :" + req.query);
})
